﻿namespace TH6.Models.GetProducts
{
    public class IGetProductsReponsitory
    {
        Task<IEnumerable<Product>> GetProductsAsync();
    }
}
